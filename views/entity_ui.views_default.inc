<?php

/**
 * Implements hook_views_default_views().
 */
function entity_ui_views_default_views() {
  $views = array();

  // Build a default administrative view per entity.
  foreach (entity_get_info() as $entity_type => $entity_info) {
    if (!isset($entity_info['entity ui']['admin view'])) {
      continue;
    }

    $admin_view_info = $entity_info['entity ui']['admin view'];

    $view = new view;
    $view->name = 'entity_ui_' . $entity_type;
    $views[$view->name] = $view;
    $view->description = '';
    $view->tag = '';
    $view->base_table = $entity_info['base table'];
    $view->human_name = $entity_info['label'];
    $view->core = 7;
    $view->api_version = '3.0-alpha1';

    /* Display: Defaults */
    $handler = $view->new_display('default', 'Defaults', 'default');
    $handler->display->display_options['title'] = $entity_info['label'];

    if (isset($admin_view_info['access'])) {
      $handler->display->display_options['access'] = $admin_view_info['access'];
    }
    else {
      $handler->display->display_options['access'] = array(
        'type' => 'perm',
        'perm' => 'administer site configuration',
      );
    }

    if (isset($admin_view_info['pager'])) {
      $handler->display->display_options['pager'] = $admin_view_info['pager'];
    }
    else {
      $handler->display->display_options['pager'] = array('type' => 'full');
    }

    if (isset($admin_view_info['style'])) {
      $handler->display->display_options['style_plugin'] = $admin_view_info['style']['plugin'];
      $handler->display->display_options['style_options'] = $admin_view_info['style']['options'];
    }
    else {
      $handler->display->display_options['style_plugin'] = 'table';
      $handler->display->display_options['style_options'] = array();

      $views_data = views_fetch_data($entity_info['base table']);
      $properties = entity_get_property_info($entity_type);

      foreach ($properties['properties'] as $name => $property_info) {
        // Remove the properties that are not exposed to Views.
        if (!isset($views_data[$name]['field']['handler'])) {
          continue;
        }

        $handler->display->display_options['style_options']['columns'][$name] = $name;
        $handler->display->display_options['style_options']['info'][$name] = array(
          'sortable' => 1,
          'default_sort_order' => 'asc',
          'align' => '',
          'separator' => '',
        );
        $handler->display->display_options['fields'][$name] = array(
          'id' => $name,
          'table' => $entity_info['base table'],
          'field' => $name,
        );
      }
    }

    // Add a entity operations set of links.
    $handler->display->display_options['fields']['entity_operations'] = array(
      'id' => 'entity_operations',
      'table' => $entity_info['base table'],
      'field' => 'entity_operations',
      'label' => 'Operations',
    );

    /* Display: Page */
    $handler = $view->new_display('page', 'Page', 'page');

    $handler->display->display_options['path'] = $admin_view_info['path'];
    $handler->display->display_options['menu'] = $admin_view_info['menu'];
    $handler->display->display_options['tab_options'] = $admin_view_info['tab_options'];
  }

  return $views;
}
