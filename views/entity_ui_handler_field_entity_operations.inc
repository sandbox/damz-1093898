<?php

/**
 * Field handler to present a product's operations links.
 */
class entity_ui_handler_field_entity_operations extends views_handler_field {
  function render($values) {
    // TODO: load the entities in bulk in pre_render().
    $entities = entity_load($this->definition['entity type'], array($values->{$this->field_alias}));

    if ($entities) {
      $entity = reset($entities);
      $uri = entity_uri($this->definition['entity type'], $entity);

      $links = menu_contextual_links('entity_ui', $uri['path'], array());

      if (!empty($links)) {
        return theme('links', array('links' => $links, 'attributes' => array('class' => array('links', 'inline', 'operations'))));
      }
    }
  }
}