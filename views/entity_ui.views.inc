<?php

/**
 * Implements hook_views_data()
 */
function entity_ui_views_data_alter(&$data) {
  foreach (entity_get_info() as $entity_type => $entity_info) {
    if (!isset($data[$entity_info['base table']])) {
      continue;
    }

    $data[$entity_info['base table']]['entity_operations'] = array(
      'field' => array(
        'field' => $entity_info['entity keys']['id'],
        'title' => t('Operation links'),
        'help' => t('Display all the available operations links for the entity.'),
        'handler' => 'entity_ui_handler_field_entity_operations',
        'entity type' => $entity_type,
      ),
    );
  }
}
